# Aimesoft - Deep Reinforcement Learning for Stock trading

This project created a trading bot using Deep Reinforcement Algorithm for trading in portfolio management. The reward for agents is the net unrealized (meaning the stocks are still in portfolio and not cashed out yet) profit evaluated at each action step.


Key assumptions and limitations of the current framework:
- trading has no impact on the market
- only single stock type is supported
- only 3 basic actions: buy, hold, sell (no short selling or other complex actions)
- the agent performs only 1 action for portfolio reallocation at the end of each trade day
- all reallocations can be finished at the closing prices
- no missing data in price history
- no transaction cost

State of a stock environment include:
- `window_size`: normalized adjacent of closed prices of `window_size` previous days.
- the logarith of `[stock_price, balance, num_holding]`.

The project implemented Deep Reinforecement Learning from scratch, so there were many things that still didn't finish.
- Algorithm isn't stable, parameters should be tuned more.
- Many strong algorithm haven't implemented yet.
- Project was designed for easily extendable and scalable.

## Data
Data in this project are from QuyentPT's StockPred project. 

## Train

`python -m src.training.train --model_name DDQN --stock_name 1400 --window-size 10`

## Eval

`python -m src.training.eval --model_to_load DQN_ep-10_stock-1332`

## Note:
The result of trading actions still have randomness, the algorithm's unstable, need to be improved.