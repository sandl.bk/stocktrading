from abc import abstractmethod
import torch


# logging = get_log()
class StrategyTransaction:
    @abstractmethod
    def buy(self, t, portfolio, stock_prices):
        pass

    @abstractmethod
    def sell(self, t, portfolio, stock_prices):
        pass

    @abstractmethod
    def hold(self, t, portfolio, stock_prices, actions):
        pass

    def build_reward(self, t, portfolio, stock_prices):
        # throws an error when t vs len(stock_prices) not match
        if t != len(portfolio.portfolio_values):
            raise Exception(f"Index t and length of portfolio_values ({t}, {len(portfolio.portfolio_values)}) don't match")
        # return prev and cur portfolio value
        # cur_portfolio_value doesn't change after do an action
        previous_portfolio_value = portfolio.portfolio_values[t - 1]
        current_portfolio_value = len(portfolio.inventory) * stock_prices[t] + portfolio.balance
        # logging.debug(f'dcm ---- {current_portfolio_value-previous_portfolio_value}')
        return {'reward': current_portfolio_value - previous_portfolio_value, 'current_portfolio_value': current_portfolio_value,
                'previous_portfolio_value': previous_portfolio_value}


class TrainStrategyTransaction(StrategyTransaction):
    def hold(self, t, portfolio, stock_prices, actions):
        respond = super().build_reward(t, portfolio, stock_prices)
        respond['log'] = 'Holding action'
        # encourage selling for profit and liquidity
        next_prob_action = torch.argsort(actions)[1]
        if next_prob_action == 2 and len(portfolio.inventory) > 0:
            max_profit = stock_prices[t] - min(portfolio.inventory)
            if max_profit > 0:
                return self.sell(t, portfolio, stock_prices)
            else:
                return respond
        else:
            return respond
        # return respond

    def buy(self, t, portfolio, stock_prices):
        respond = super().build_reward(t, portfolio, stock_prices)

        if portfolio.balance > stock_prices[t]:
            portfolio.balance -= stock_prices[t]
            portfolio.inventory.append(stock_prices[t])
            respond['log'] = 'Buy: ${:.2f}'.format(stock_prices[t])
        else:
            # punish for not accepted actions.
            respond['reward'] = -1
            respond['log'] = 'Not enough balance'
        return respond

    def sell(self, t, portfolio, stock_prices):
        respond = super().build_reward(t, portfolio, stock_prices)

        if len(portfolio.inventory) > 0:
            portfolio.balance += stock_prices[t]
            bought_price = portfolio.inventory.pop(0)
            profit = stock_prices[t] - bought_price
            respond['log'] = 'Sell: ${:.2f} | Profit: ${:.2f}'.format(stock_prices[t], profit), profit
        else:
            # punish for not accepted actions.
            respond['reward'] = -1
            respond['log'] = 'No inventory available'
        return respond


class EvaluateStrategyTransaction(StrategyTransaction):
    def hold(self, t, portfolio, stock_prices, actions):
        respond = super().build_reward(t, portfolio, stock_prices)
        respond['log'] = 'Hold'
        return respond

    def buy(self, t, portfolio, stock_prices):
        respond = super().build_reward(t, portfolio, stock_prices)
        portfolio.balance -= stock_prices[t]
        portfolio.inventory.append(stock_prices[t])
        portfolio.buy_dates.append(t)
        respond['log'] = 'Buy: ${:.2f}'.format(stock_prices[t])
        return respond

    def sell(self, t, portfolio, stock_prices):
        respond = super().build_reward(t, portfolio, stock_prices)
        portfolio.balance += stock_prices[t]
        bought_price = portfolio.inventory.pop(0)
        portfolio.sell_dates.append(t)
        profit = stock_prices[t] - bought_price
        respond['log'] = 'Sell: ${:.2f} | Profit: ${:.2f}'.format(stock_prices[t], profit), profit
        return respond
