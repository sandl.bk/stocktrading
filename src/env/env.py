from src.env.transaction import StrategyTransaction
from src.utils import generate_combined_state, Portfolio


class Env:
    def __init__(self, stock_prices, window_size, strategy: StrategyTransaction):
        self.stock_prices = stock_prices
        self.strategy = strategy
        self.portfolio = Portfolio()
        self.window_size = window_size
        self.index = -1

    def generate_combined_state(self):
        if self.index >= len(self.stock_prices):
            raise ValueError
        self.index += 1
        respond = generate_combined_state(self.index, self.window_size, self.stock_prices,
                                          self.portfolio.balance, len(self.portfolio.inventory))
        return respond

    def reset(self):
        self.portfolio.reset_portfolio()
        self.index = -1

    def take_action(self, type: str, **kwargs):
        # type is an action
        if type == 'hold':
            respond = self.strategy.hold(self.index, self.portfolio, self.stock_prices, kwargs['actions'])
        elif type == 'buy':
            respond = self.strategy.buy(self.index, self.portfolio, self.stock_prices)
        elif type == 'sell':
            respond = self.strategy.sell(self.index, self.portfolio, self.stock_prices)
        else:
            raise NotImplementedError
        # update
        self.portfolio.portfolio_values.append(respond['current_portfolio_value'])
        self.portfolio.return_rates.append((respond['current_portfolio_value'] - respond['current_portfolio_value']) / respond['previous_portfolio_value'])
        return respond
