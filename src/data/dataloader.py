import pandas as pd


class DataTrading:
    def __init__(self, name_stock, stock_price_type='Close', train_ratio=0.8, eval_ratio=0.1):
        self.df = pd.read_csv(f'./data/trading_days/{name_stock}.csv')
        self.stock_price_type = stock_price_type
        assert self.stock_price_type in self.df.columns.values
        self.date = self.df['Date'].to_numpy()
        self.price = self.df[self.stock_price_type].to_numpy()
        self.length = self.date.shape[0]
        self.train = int(train_ratio * self.length)
        self.eval = self.train + int(eval_ratio * self.length)
        self.test = self.length

    def date_by_id(self, idx):
        return self.date[idx]

    def get_data(self, mode):
        assert mode in ['train', 'test', 'eval']
        if mode == 'train':
            return self.price[:self.train]
        elif mode == 'eval':
            return self.price[self.train:self.eval]
        else:
            return self.price[self.eval:self.length]

    def get_df(self, mode):
        assert mode in ['train', 'test', 'eval']
        if mode == 'train':
            return self.df[:self.train]
        elif mode == 'eval':
            return self.df[self.train:self.eval]
        else:
            return self.df[self.eval:self.length]
