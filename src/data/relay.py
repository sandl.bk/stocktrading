from collections import namedtuple, deque
import random
import torch

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'done'))


class ReplayMemory:
    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """ Save a transition """
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        random_batch = random.sample(self.memory, batch_size)
        inp = {}
        for key in Transition._fields:
            if isinstance(getattr(random_batch[0], key), tuple):
                seq = [None] * 2
                seq[0] = [getattr(i, key)[0] for i in random_batch]
                seq[1] = [getattr(i, key)[1] for i in random_batch]
                inp[key] = (torch.cat(seq[0], dim=0), torch.cat(seq[1], dim=0))
            elif key == 'action' or key == 'reward':
                seq = [getattr(i, key).unsqueeze(0) for i in random_batch]
                inp[key] = torch.cat(seq, dim=0)
            else:
                seq = [getattr(i, key) for i in random_batch]
                inp[key] = torch.cat(seq, dim=0)
        return inp

    def __len__(self):
        return len(self.memory)
