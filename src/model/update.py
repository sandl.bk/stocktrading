from typing import Iterable
import torch.nn as nn
import torch


def polyak_update(
    params: Iterable[nn.Parameter],
    target_params: Iterable[nn.Parameter],
    tau: float
) -> None:
    with torch.no_grad():
        for param, target_param in zip(params, target_params):
            target_param.data.mul_(1 - tau)
            torch.add(target_param.data, param.data, alpha=tau, out=target_param.data)
