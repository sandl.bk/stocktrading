import torch
import torch.nn as nn
import random
from src.model.update import polyak_update
from src.data import relay


class DQNParameter:
    def __init__(self, **kwargs):
        self.action_size = 3    # ['hold', 'buy', 'sell']
        self.window_size = 10
        self.gamma = 0.99  # affinity for long term reward
        self.epsilon = 1.0
        self.epsilon_min = 0.1
        self.epsilon_decay = kwargs['epsilon_decay'] if 'epsilon_decay' in kwargs else None
        self.learning_rate = 1e-5
        self.dropout = 0
        self.batch_first = True
        self.num_lstm_layer = 1
        self.bi_lstm = True
        self.hidden_size = 64
        self.activate_func = 'relu'
        self.reset_every = 1000
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.addition_features = 3  # current price stock, balance, num holding
        self.window_size = kwargs['window_size']
        self.tau = 1.


class DeepQN(nn.Module):
    def __init__(self, param, **kwargs):
        super(DeepQN, self).__init__()
        self.param = param
        self.epsilon = param.epsilon
        self.epsilon_min = param.epsilon_min
        self.epsilon_decay = param.epsilon_decay
        self.lstm = nn.LSTM(input_size=2, hidden_size=param.hidden_size, num_layers=param.num_lstm_layer,
                            batch_first=param.batch_first, dropout=param.dropout, bidirectional=param.bi_lstm)
        # if param.bi_lstm:
        #     self.conv1 = nn.Conv1d(2*param.hidden_size, param.hidden_size, 3, padding=1)
        # else:
        #     self.conv1 = nn.Conv1d(param.hidden_size, param.hidden_size, 3, padding=1)
        # if param.bi_lstm:
        #     self.linear1 = nn.Linear(in_features=param.hidden_size * 2 + param.addition_features,
        #                              out_features=param.hidden_size * 2)
        # else:
        #     self.linear1 = nn.Linear(in_features=param.hidden_size + param.addition_features,
        #                              out_features=param.hidden_size * 2)
        self.linear1 = nn.Linear(13, param.hidden_size * 2)
        if param.activate_func == 'relu':
            self.act_func = nn.ReLU()
        elif param.activate_func == 'sigmoid':
            self.sigmoid = nn.Sigmoid()
        else:
            raise NotImplementedError
        self.linear2 = nn.Linear(in_features=param.hidden_size * 2, out_features=param.hidden_size)
        self.linear3 = nn.Linear(in_features=param.hidden_size, out_features=param.action_size)
        # self.portfolio = utils.Portfolio()
        self.memory = relay.ReplayMemory(capacity=10000)
        self.loss = nn.SmoothL1Loss()
        self.dropout = nn.Dropout(param.dropout)
        self.softmax = nn.Softmax(dim=1)
        if 'logging' in kwargs:
            self.logging = kwargs['logging']
        else:
            self.logging = None
            assert 'logging' in kwargs
        self.optimizer = None

    def forward(self, x, addition_features):
        # [batch_size, seq_len, 2 * hidden_size]
        # just for bebug
        # batch_size = x.shape[0]

        # if x.shape[0] > 1:
        #     debug = 0
        # x = torch.cat((x, x), dim=-1)
        # print(x.shape)
        # x = self.lstm(x)[0][:, -1, :]
        x = x.squeeze(dim=-1)
        x = torch.cat((x, addition_features), dim=1)

        x = self.linear1(x)
        x = self.dropout(x)
        x = self.act_func(x)

        x = self.linear2(x)
        x = self.dropout(x)
        x = self.act_func(x)

        x = self.linear3(x)
        return x

    def act(self, x, addition_features, e_greedy=True):
        if e_greedy and random.random() <= self.epsilon:
            return torch.tensor(random.randrange(self.param.action_size))
        options = self(x, addition_features)[0]
        return torch.argmax(options, dim=-1)

    def update_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def save(self, episode, path='./saved_model/DQN'):
        self.model.save(path + '_' + str(episode))

    def load(self, name):
        self.model.load(name)

    def update_target(self, target):
        polyak_update(self.parameters(), target.parameters(), self.param.tau)

    # def reset(self):
    #     self.portfolio.reset_portfolio()

    def reset_eps(self):
        self.epsilon = 1.0

    def decay_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
            self.logging.info(f'E-greedy epsilon := {self.epsilon}')

    def experience_replay(self, target_model, batch_size):
        # for p in target_model.parameters():
        #     print(p.data)
        #     break
        mini_batch = self.memory.sample(batch_size)
        mini_batch['done'] = mini_batch['done'].unsqueeze(1)
        mini_batch['action'] = mini_batch['action'].unsqueeze(1)
        # x_train, y_train = [], []
        with torch.no_grad():
            # Compute the next Q-values using the target network
            next_q_values = target_model(mini_batch['next_state'][0], mini_batch['next_state'][1])
            # Follow greedy policy: use the one with the highest value
            next_q_values, _ = next_q_values.max(dim=1)

            next_q_values = next_q_values.unsqueeze(1)
            # mini_batch['done'] = mini_batch['done'].unsqueeze(1)
            target_q_values = mini_batch['reward'] + self.param.gamma * mini_batch['done'] * next_q_values
        q_values = self(mini_batch['state'][0], mini_batch['state'][1])
        q_values_with_action = torch.gather(q_values, dim=1, index=mini_batch['action'].long())
        loss = self.loss(q_values_with_action, target_q_values)
        self.decay_epsilon()
        return loss


class DoubleDeepQN(DeepQN):
    def __init__(self, param, **kwargs):
        super().__init__(param, **kwargs)

    def experience_replay(self, target_model, batch_size):
        mini_batch = self.memory.sample(batch_size)
        mini_batch['done'] = mini_batch['done'].unsqueeze(1)
        mini_batch['action'] = mini_batch['action'].unsqueeze(1)
        # x_train, y_train = [], []
        q_values = self(mini_batch['state'][0], mini_batch['state'][1])
        actions = q_values.detach()
        with torch.no_grad():
            # take action from policy network.
            actions = torch.argmax(actions, dim=1).unsqueeze(1)
            next_q_values = target_model(mini_batch['next_state'][0], mini_batch['next_state'][1])
            # get next_q_values from actions
            next_q_values = torch.gather(next_q_values, dim=1, index=actions.long())
            # mini_batch['done'] = mini_batch['done'].unsqueeze(1)
            target_q_values = mini_batch['reward'] + self.param.gamma * mini_batch['done'] * next_q_values
        q_values_with_action = torch.gather(q_values, dim=1, index=mini_batch['action'].long())
        loss = self.loss(q_values_with_action, target_q_values)
        self.decay_epsilon()
        return loss
