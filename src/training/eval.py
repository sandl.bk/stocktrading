import argparse
import logging
from src.env.transaction import EvaluateStrategyTransaction
from src.env.env import Env
from src.utils import evaluate_portfolio_performance, plot_all
from src.data.dataloader import DataTrading
import torch
import importlib
from src.model.DQN import DQNParameter
from tqdm import tqdm


parser = argparse.ArgumentParser(description='command line options')
parser.add_argument('--model_to_load', action='store', dest='model_to_load', default='DQN_ep-10_stock-1332', help='model name saved')
parser.add_argument('--init_balance', action='store', dest='init_balance', default=50000, help='init balance')
parser.add_argument('--display', action='store', dest='display', default=True)
arg = parser.parse_args()

action_dict = {0: 'hold', 1: 'buy', 2: 'sell'}
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model_name = arg.model_to_load.split('_')[0]
stock_name = arg.model_to_load.split('-')[-1]

if 'DQN' in model_name:
    model = importlib.import_module('src.model.DQN')
else:
    raise NotImplementedError

checkpoint = torch.load(f'./saved_model/{arg.model_to_load}.pt')
window_size = checkpoint['window_size']
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
reward = 0
# configure logging
logging.basicConfig(filename=f'./logs/{model_name}_evaluating_{stock_name}.log', filemode='w',
                    format='[%(asctime)s.%(msecs)03d %(filename)s:%(lineno)3s] %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)


def main():
    global window_size, checkpoint, stock_name, device, model_name
    if model_name == 'DQN':
        param = DQNParameter(window_size=window_size)
        target_model = model.DeepQN(param, logging=logging)
        target_model.load_state_dict(checkpoint['model_state_dict'])
        target_model.to(device)
        target_model.train()
    elif model_name == 'DDQN':
        param = DQNParameter(window_size=window_size)
        target_model = model.DoubleDeepQN(param, logging=logging)
        target_model.load_state_dict(checkpoint['model_state_dict'])
        target_model.to(device)
        target_model.train()

    dataloader = DataTrading(stock_name, train_ratio=0.8, eval_ratio=0.1)
    stock_prices = dataloader.get_data('eval')
    trading_period = len(stock_prices) - 1

    env = Env(stock_prices, window_size, EvaluateStrategyTransaction())

    state = env.generate_combined_state()
    state = (state[0].to(device), state[1].to(device))
    for t in tqdm(range(1, trading_period + 1)):
        actions = target_model(state[0], state[1])[0]
        action = torch.argmax(actions, dim=-1).item()

        logging.info(f'actions: {actions[0]}, {actions[1]}, {actions[2]}')
        print('chosen action:', action)

        next_state = env.generate_combined_state()
        next_state = (next_state[0].to(device), next_state[1].to(device))

        # execute position
        logging.info(f'Step: {t}')
        assert action == torch.argmax(actions).item()
        if action == 0:
            respond = env.take_action(action_dict[action], actions=actions)
        elif action == 1 and env.portfolio.balance > stock_prices[t]:
            respond = env.take_action(action_dict[action])
        elif action == 2 and len(env.portfolio.inventory) > 0:
            respond = env.take_action(action_dict[action])
        else:
            respond = env.take_action('hold', actions=actions)
        logging.info(respond['log'])

        state = next_state
        done = torch.zeros(1, device=device) if t == trading_period - 1 else torch.ones(1, device=device)
        if done == 0:
            evaluate_portfolio_performance(env.portfolio, logging)

    if arg.display:
        plot_all(stock_name, env.portfolio, arg.model_to_load, dataloader.get_df('eval'))


if __name__ == '__main__':
    main()
