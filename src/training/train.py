import logging
from src.env.env import Env
from src.env.transaction import TrainStrategyTransaction
from src.utils import evaluate_portfolio_performance, plot_portfolio_returns_across_episodes
from src.model.DQN import DeepQN, DQNParameter, DoubleDeepQN
from src.data.dataloader import DataTrading
import torch
from tqdm import tqdm
import numpy as np
import argparse


# class Argument:
#     def __init__(self, model_name, stock_name, window_size, init_balance, num_episode, batch_size):
#         self.model_name = model_name
#         self.stock_name = stock_name
#         self.window_size = window_size
#         self.init_balance = init_balance
#         self.num_episode = num_episode
#         self.batch_size = batch_size
#         self.lr = 0.01


parser = argparse.ArgumentParser(description='command line options')
parser.add_argument('--model_name', action="store", dest="model_name", default='DDQN', help="model name")
parser.add_argument('--stock_name', action='store', dest='stock_name', default='1400', help='stock name')
parser.add_argument('--window_size', action='store', dest='window_size', type=int, default=10, help='window size')
parser.add_argument('--init_balance', action='store', dest='init_balance', type=int, default=50000, help='init balance')
parser.add_argument('--num_episode', action='store', dest='num_episode', type=int, default=15, help='num episode')
parser.add_argument('--batch_size', action='store', dest='batch_size', type=int, default=32, help='batch size')
parser.add_argument('--lr', action='store', dest='lr', type=float, default=0.01, help='learning rate')
parser.add_argument('--stop_greedy', action='store', dest='stop_greedy', type=float, default=0.8, help='stop epsilon greedy')
arg = parser.parse_args()

# configure logging
logging.basicConfig(filename=f'./logs/{arg.model_name}_training_{arg.stock_name}_ep-{arg.num_episode}.log', filemode='w',
                    format='[%(asctime)s.%(msecs)03d %(filename)s:%(lineno)3s] %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
action_dict = {0: 'hold', 1: 'buy', 2: 'sell'}
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
num_experience_play = 0
returns_across_episodes = []


def init_log(trading_period):
    logging.info(f'Trading Object:           {arg.stock_name}')
    logging.info(f'Trading Period:           {trading_period} days')
    logging.info(f'Window Size:              {arg.window_size} days')
    logging.info(f'Training Episode:         {arg.num_episode}')
    logging.info(f'Model Name:               {arg.model_name}')
    logging.info(f'Batch Size:               {arg.batch_size}')
    logging.info('Initial Portfolio Value: ${:,}'.format(arg.init_balance))


def main():
    global num_experience_play

    dataloader = DataTrading(arg.stock_name, train_ratio=0.8, eval_ratio=0.2)
    stock_prices_train = dataloader.get_data('train')
    trading_period = len(stock_prices_train) - 1
    # calculation for epsilon_decay
    expect_batch = trading_period * arg.num_episode * arg.stop_greedy
    epsilon_decay = 0.1 ** float(1 / expect_batch)

    if arg.model_name == 'DQN':
        param = DQNParameter(window_size=arg.window_size, epsilon_decay=epsilon_decay)
        policy_model = DeepQN(param, logging=logging).to(device)
        target_model = DeepQN(param, logging=logging).to(device)
        policy_model.train()
        target_model.eval()
    elif arg.model_name == 'DDQN':
        param = DQNParameter(window_size=arg.window_size, epsilon_decay=epsilon_decay)
        policy_model = DoubleDeepQN(param, logging=logging).to(device)
        target_model = DoubleDeepQN(param, logging=logging).to(device)
        policy_model.train()
        target_model.eval()
    else:
        raise NotImplementedError
    policy_model.optimizer = torch.optim.Adam(policy_model.parameters(), policy_model.param.learning_rate)
    optimizer = policy_model.optimizer
    env = Env(stock_prices_train, arg.window_size, TrainStrategyTransaction())

    init_log(trading_period)

    for ep in tqdm(range(1, arg.num_episode + 1)):
        logging.info(f'\n----------- Episode {ep}/{arg.num_episode} -----------')
        # reset to initial balance
        env.reset()
        # if ep < int(0.8 * arg.num_episode):
        #     policy_model.reset_eps()

        state = env.generate_combined_state()
        state = (state[0].to(device), state[1].to(device))
        for t in range(1, trading_period + 1):
            if t % 100:
                logging.info(f'\n----------- Period {t}/{trading_period} -----------')
            reward = 0
            next_state = env.generate_combined_state()
            next_state = (next_state[0].to(device), next_state[1].to(device))
            # convert policy model to eval mode
            policy_model.eval()
            q_actions_value = policy_model(state[0], state[1])[0]
            action = policy_model.act(state[0], state[1], e_greedy=True)
            # in case: action is taken by e-greedy -> convert to device
            action = action.to(device)
            # convert policy model back to train mode
            policy_model.train()

            logging.info('Step: {}\tHold signal: {:.4} \tBuy signal: {:.4} \tSell signal: {:.4}'
                         .format(t, q_actions_value[0], q_actions_value[1], q_actions_value[2]))

            if action != torch.argmax(q_actions_value).item():
                logging.info(f"\t\t'{action_dict[action.item()]}' is an exploration.")

            respond = env.take_action(action_dict[action.item()], actions=q_actions_value)
            reward = respond['reward']
            logging.info(f'reward dcm: {reward}')
            if respond['log'][:4] == 'sell':
                action = torch.tensor([2]).to(device)
            logging.info(respond['log'])

            done = torch.zeros(1, device=device) if t == trading_period - 1 else torch.ones(1, device=device)
            reward = np.array([reward], dtype=np.float32)
            policy_model.memory.push(state, action, next_state, torch.from_numpy(reward).to(device), done)
            state = next_state

            if len(policy_model.memory) >= arg.batch_size:
                # update target model
                if num_experience_play % policy_model.param.reset_every == 0:
                    logging.info('update policy model to target model')
                    policy_model.update_target(target_model)
                num_experience_play += 1
                optimizer.zero_grad()
                loss = policy_model.experience_replay(target_model, arg.batch_size)
                loss.backward()
                optimizer.step()
                loss_value = loss.item()
                if num_experience_play % 100 == 0:
                    logging.info(
                        'Episode: {}\tLoss: {:.2f}\tAction: {}\tReward: {:.2f}\tBalance: {:.2f}\tNumber of Stocks: {}'.format(
                            ep, loss_value, action_dict[action.item()], reward[0], env.portfolio.balance, len(env.portfolio.inventory)))
            if done == 0:
                portfolio_return = evaluate_portfolio_performance(env.portfolio, logging)
                returns_across_episodes.append(portfolio_return)
        # if ep % 10 == 0 and ep != 0:
    torch.save({
        'model_state_dict': target_model.state_dict(),
        'window_size': arg.window_size},
        f'./saved_model/{arg.model_name}_ep-{ep}_stock-{arg.stock_name}.pt')

    plot_portfolio_returns_across_episodes(arg.model_name + '_' + arg.stock_name, returns_across_episodes)


if __name__ == '__main__':
    main()
